import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-logout',
  templateUrl: './template.html',
  styleUrls: ['./style.styl'],
})
export class LogoutComponent implements OnInit {
  constructor() {}

  redirect() {
    // setTimeout(() => (window.location.assign('...')), 2000);
  }

  ngOnInit() {}
}
