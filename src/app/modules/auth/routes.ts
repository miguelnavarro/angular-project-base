import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  LogoutComponent,
} from './components';

const routes: Routes = [
  {
    path: 'logout',
    component: LogoutComponent,
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {}
