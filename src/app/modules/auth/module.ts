import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from 'modules/core/module';
import { RoutingModule } from './routes';
import {
  LogoutComponent,
} from './components';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    RoutingModule,
  ],
  declarations: [
    LogoutComponent,
  ],
  exports: [],
  providers: [],
})
export class AuthModule {}
