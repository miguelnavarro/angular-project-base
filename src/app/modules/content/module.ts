import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from 'modules/core/module';
import {
  LayoutComponent,
} from './components';
import { RoutingModule } from './routes';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    RoutingModule,
  ],
  declarations: [
    LayoutComponent,
  ],
  exports: [],
  providers: [
  ],
})
export class ContentModule {}
