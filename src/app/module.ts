import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { AppComponent } from './';
import { RoutingModule } from './routes';
import { CoreModule } from 'modules/core/module';
import { AuthModule } from 'modules/auth/module';
import { ContentModule } from 'modules/content/module';
import { HttpService } from 'services';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpModule,
    CoreModule,
    AuthModule,
    ContentModule,
    RoutingModule,
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    HttpService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
