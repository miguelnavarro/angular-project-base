import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/finally';
import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { cond, equals, always, T, reduce, toPairs } from 'ramda';
import { resolveApiRoute } from 'scripts/utils';

@Injectable()
export class HttpService {
  constructor(private http: Http, private router: Router) {}

  get(
    routeIdentifier: string,
    body?: object,
    willSendToken?: boolean,
    options?: RequestOptions
  ): Observable<any> | any {
    return this.request('get', routeIdentifier, body, willSendToken, options);
  }

  post(
    routeIdentifier: string,
    body?: object,
    willSendToken?: boolean,
    options?: RequestOptions
  ): Observable<any> | any {
    return this.request('post', routeIdentifier, body, willSendToken, options);
  }

  private request(
    method: string,
    routeIdentifier: string,
    body?: object,
    willSendToken?: boolean,
    options?: RequestOptions
  ): Observable<any> | any {
    const route: string = resolveApiRoute(routeIdentifier);
    const dataForUrl: string =
      reduce(
        (acc: string, value: any) => `${acc}${value[0]}=${value[1]}&`,
        '?',
        toPairs(body)
      ) || '';
    const token: string = localStorage.getItem('token') || '';
    const headers: Headers = willSendToken
      ? new Headers({ Authorization: token })
      : new Headers();
    const requestOptions: RequestOptions =
      options || new RequestOptions({ headers });
    return cond([
      [
        equals('get'),
        () =>
          this.intercept(
            this.http
              .get(route + dataForUrl, requestOptions)
              .map(this.extractData)
          ),
      ],
      [
        equals('post'),
        () =>
          this.intercept(
            this.http.post(route, body, requestOptions).map(this.extractData)
          ),
      ],
    ])(method);
  }

  private extractData(res: Response | any): any {
    const message = res.json() || {};
    return message;
  }

  private intercept(observable: Observable<any>): Observable<any> {
    return observable.catch((error: Response | any): ErrorObservable => {
      const { status } = error;
      const body = error.json() || {};
      const message = cond([
        [equals(400), always(body)],
        [equals(401), () => this.unauthorised()],
        [equals(403), () => this.forbidden(body)],
        [T, always(`Error ${status}: ${body}`)],
      ])(status);
      return Observable.throw(message);
    });
  }

  private unauthorised() {
    this.router.navigate(['/forbidden']);
  }
  private forbidden(body) {
    window.alert(body);
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.router.navigate(['/forbidden']);
  }
}
