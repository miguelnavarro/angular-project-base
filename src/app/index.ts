import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './template.html',
  styleUrls: ['./style.styl'],
  providers: [],
})
export class AppComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
