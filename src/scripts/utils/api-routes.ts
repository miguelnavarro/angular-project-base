import { default as api } from 'src/config/api.json';

const { host, basePath, routes } = api;

export const resolveApiRoute = (identifier: string): string => {
  return `${host}${basePath}${routes[identifier]}`;
};
